To continue the paper:
- Install Latax:
	- sudo apt-get install texlive-full
	- sudo apt-get install texstudio

- download aalto logo package from 
https://wiki.aalto.fi/download/attachments/49383512/aaltologo.zip
- Unzip the package and copy the folder to /user/share/texmf/tex/latex/ 
- then run command "sudo texhash" to update it